#!/bin/bash

# Aaron Wei
# 20181002
# Automation create VM on Linux KVM

read -p "Please enter VM name: " VMName
TESTING=$(virsh list --all | grep "$VMName" |sed "s/^.*-     //g" |sed "s/ *shut off.*$//g")
while [[ "$VMName" = "$TESTING" ]]
do
  echo "This VMName already exists! Please change VMName!"
  read -p "Please enter VM name: " VMName
done
read -p "Please enter disk size (GB): " DISK_SIZE
until [[ "$DISK_SIZE" =~ ^-?[0-9]+$ ]]
do
  echo "Please input correct Disk size!"
  read -p "Please enter disk size (GB): " DISK_SIZE
done
read -p "Please enter memory size (MB): " RAM
until [[ "$RAM" =~ ^-?[0-9]+$ ]]
do
  echo "Please input correct Memory size!"
  read -p "Please enter Memory size (MB): " RAM
done
read -p "Please enter number of vCPUs (1~4) : " VCPUS
until [[ "$VCPUS" =~ ^-?[0-9]+$ ]]
do
  echo "Please input correct vCPUs amount!"
  read -p "Please enter vCPUs (1~4): " VCPUS
done
read -p "Are you sure create this VM? (Y/N) " yn
DISK_FORMAT="qcow2" # Define VM disk image format.. qcow2|img
DISK_PATH="/var/lib/libvirt/images" # Define path where VM disk images are stored
EXTRA_ARGS="console=ttyS0,115200n8 serial"
LOCATION="http://ftp.cse.yzu.edu.tw/ubuntu/dists/xenial/main/installer-amd64/"
NETWORK_BRIDGE="br0" # virbr0
OS_TYPE="linux" # Define OS type to install... linux|windows
OS_VARIANT="ubuntu15.04" # ubuntu14.04|debian8|centos7.0
PRESEED_FILE="/root/automation/preseeds/ubuntu1604/preseed.cfg" # Define preseed file for Debian based installs if desired
PRESEED_INSTALL="true" # Define if preseed install is desired
#RAM="512" # Define memory to allocate to VM in MB... 512|1024|2048
#VCPUS="1" # Define number of vCPUs to allocate to VM
#VMName="ubuntu1604" # Define name of VM to create
if [[ "$yn" == "Y" ]] || [[ "$yn" == "y" ]]; then
  if [[ "$PRESEED_INSTALL" = false ]]; then
    virt-install \
      --name $VMName \
      --ram $RAM \
      --disk path=$DISK_PATH/$VMName.$DISK_FORMAT,size=$DISK_SIZE \
      --vcpus $VCPUS \
      --os-type $OS_TYPE \
      --os-variant $OS_VARIANT \
      --network bridge=$NETWORK_BRIDGE \
      --graphics none \
      --console pty,target_type=serial \
      --location $LOCATION \
      --extra-args "$EXTRA_ARGS"
  else
    virt-install \
      --name $VMName \
      --ram $RAM \
      --disk path=$DISK_PATH/$VMName.$DISK_FORMAT,size=$DISK_SIZE \
      --vcpus $VCPUS \
      --os-type $OS_TYPE \
      --os-variant $OS_VARIANT \
      --network bridge=$NETWORK_BRIDGE \
      --graphics none \
      --console pty,target_type=serial \
      --location $LOCATION \
      --initrd-inject=$PRESEED_FILE \
      --noreboot \
      --extra-args "$EXTRA_ARGS"
  fi
elif [[ "$yn" == "N" ]] || [[ "$yn" == "n" ]]; then
  echo -e "Abort! Thank you!"
fi
