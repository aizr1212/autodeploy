#!/bin/bash
#Delete VM on KVM host.
#Aaron Wei 2018/10/9

read -p "Please input VM name that you want to delete:" host

if [[ -n "$host" ]]; then
  virsh destroy $host
  virsh undefine $host
  rm -f /var/lib/libvirt/images/$host.qcow2
  echo -e "The VM: " $host "was destroy!"
  sleep 1
  echo -e "KVM list:\n `virsh list --all`"
else
  echo "You must input a VM name!"
  exit 0
fi
